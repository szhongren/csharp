﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            System.Console.WriteLine("Type conversion is what it sounds like.");
            System.Console.WriteLine("2 kinds: implicit and explicit.");

            double d = 5678.876;

            // cast double to int
            int i = (int)d;
            System.Console.WriteLine("casting {0} as {1}\n", d, i);

            System.Console.WriteLine("Here we use the built-in functions to convert");
            int i2 = 75;
            float f = 7373.76f;
            double d2 = 888.798;
            bool b = true;

            System.Console.WriteLine(i2.ToString());
            System.Console.WriteLine(f.ToString());
            System.Console.WriteLine(d2.ToString());
            System.Console.WriteLine(b.ToString());
        }
    }
}
