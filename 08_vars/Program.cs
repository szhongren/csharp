﻿namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            System.Console.WriteLine("A var is simply a name given to a piece of data.");
            System.Console.WriteLine("Decimal data type is a precise type used for financial stuff.");
            short a;
            int b;
            double c;

            a = 10;
            b = 20;
            c = a + b;
            System.Console.WriteLine("a = {0}, b = {1}, c = {2}", a, b, c);
            System.Console.ReadLine();

            System.Console.WriteLine("lvalue is an expression that can appear on left or right side of an assignment");
            System.Console.WriteLine("rvalue is an expression that can only appear on the right side");
        }
    }
}
