﻿using System;   // using namespace

namespace ConsoleApplication    // namespace of this thing, comments are C-like
{
    public class Program        // class of this thing, OOP! class is an object
    {
        public static void Main(string[] args)  // entry point
        {
            Console.WriteLine("Hello World!");
            Rect r = new Rect(); // instantiation
            r.AcceptDetails();
            r.Display();
        }
    }

    public class Rect
    {
        // member vars
        double length;
        double width;
        public void AcceptDetails() // member function aka methods
        {
            length = 4.5;
            width = 3.5;
        }

        public double GetArea()
        {
            return length * width;
        }

        public void Display()
        {
            Console.WriteLine("{0} * {1} = {2}", length, width, GetArea());
        }
    }
}
