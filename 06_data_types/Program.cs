﻿namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            decimal dec = 1.000M; // M suffix needed here
            System.Console.WriteLine("C# has most of the typical C types, but includes a 128bit decimal type that is precise: {0}", dec);
            System.Console.WriteLine("All types derived from System.ValueType.\n");
            System.Console.WriteLine("Reference types don't have a value, but instead hold an address.");
            System.Console.WriteLine("Examples are: object, dynamic, and string.\n");
            System.Console.WriteLine("Object Type is the ultimate base class for all data types in the C# Common Type System.");
            System.Console.WriteLine("Alias for System.Object class. Can be assigned values of any type but needs to be converted.");
            System.Console.WriteLine("Conversion to Object type is boxing and conversion from is unboxing.");
            System.Console.WriteLine("Type checking takes place at compile time.\n");
            System.Console.WriteLine("Dynamic type can also store any type of value. Type checking takes place at runtime.\n");
            System.Console.WriteLine("String types can take any string, alias for System.String class.");
            System.Console.WriteLine("Can be quoted with \"\" like normal or with @.");

        }
    }
}
